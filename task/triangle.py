def is_triangle(a, b, c):
    "a,b,c sides should be bigger than zero and sum of two sides should be bigger than third one"
    'checking first condition'
    if a<=0 or b<=0 or c<=0:
        return False
    'checking second condition'
    if a+c>b and b+c>a and a+b>c:
        return True

    return False
